<?php
/**
 * Ekko functions file
 *
 * @package ekko
 * by KeyDesign
 */

 add_action( 'wp_enqueue_scripts', 'kd_enqueue_parent_theme_style', 5 );
 if ( ! function_exists( 'kd_enqueue_parent_theme_style' ) ) {
     function kd_enqueue_parent_theme_style() {
         wp_enqueue_style( 'bootstrap' );
         wp_enqueue_style( 'keydesign-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ) );
         wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('keydesign-style') );
     }
 }

 add_action( 'after_setup_theme', 'kd_child_theme_setup' );
 if ( ! function_exists( 'kd_child_theme_setup' ) ) {
     function kd_child_theme_setup() {
         load_child_theme_textdomain( 'ekko', get_stylesheet_directory() . '/languages' );
     }
 }

 // -------------------------------------
 // Edit below this line
 // -------------------------------------
 require_once 'elements/elements.php';

 // Fix for aws
function as3cf_filter_get_post_metadata( $metadata, $object_id, $meta_key, $single ) {
    $meta_filter = array('_wpb_shortcodes_custom_css', '_wpb_post_custom_css');
    if ( isset( $meta_key ) && $single && in_array($meta_key, $meta_filter)) {
        remove_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100 );
        $metadata = get_post_meta( $object_id, $meta_key, $single );
        add_filter('get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4);
        $metadata = apply_filters( 'as3cf_filter_post_local_to_s3', $metadata );
    }
    return $metadata;
}
add_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4 );

add_action( 'wp_footer', 'install_albacross');
function install_albacross() {
    echo <<<HTML
        <script>window._nQc="89110506";</script>
        <script async src="https://serve.albacross.com/track.js"></script>
    HTML;
}

add_action( 'wp_head', 'install_leadfeeder');
function install_leadfeeder() {
    echo <<<HTML
        <script> (function(ss,ex){ window.ldfdr=window.ldfdr||function(){(ldfdr._q=ldfdr._q||[]).push([].slice.call(arguments));}; (function(d,s){ fs=d.getElementsByTagName(s)[0]; function ce(src){ var cs=d.createElement(s); cs.src=src; cs.async=1; fs.parentNode.insertBefore(cs,fs); }; ce('https://sc.lfeeder.com/lftracker_v1_'+ss+(ex?'_'+ex:'')+'.js'); })(document,'script'); })('bElvO73Rqk18ZMqj'); </script>
    HTML;
}