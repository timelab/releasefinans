<?php
if (function_exists('vc_map_update')) {
    // Change maps settings
    vc_map_update( 'tek_map', array('html_template' => get_stylesheet_directory() . '/elements/map.php') ); 
    add_action( 'init', 'remove_tek_map_shortcodes',20 );
    function remove_tek_map_shortcodes() {
        remove_shortcode( 'tek_map' );
    }
}

if (function_exists('vc_add_param')) {
    /* Add option for URL to column settings */

    vc_add_param("vc_column", array(
        "type" => "vc_link",
        "class" => "",
        "heading" => "Column Link",
        "param_name" => "column_link"
    ));

    /* Add option for URL to column_inner settings */

    vc_add_param("vc_column_inner", array(
        "type" => "vc_link",
        "class" => "",
        "heading" => "Column Link",
        "param_name" => "column_link"
    ));

    add_filter('vc_shortcode_set_template_vc_column', function($template){
        return get_stylesheet_directory() . '/vc_templates/vc_column.php';
    });
}